package com.zimug.dongbb.cloud.aservice.rbac;

import feign.Logger;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication(scanBasePackages={"com.zimug.dongbb.cloud"})
@MapperScan(basePackages = {"com.zimug.dongbb.cloud.**.mapper"})
@EnableDiscoveryClient
@EnableFeignClients
public class AserviceRbacApplication {

	public static void main(String[] args) {
		SpringApplication.run(AserviceRbacApplication.class, args);
	}


	@Bean
	public Logger.Level feignLoggerLevel(){
		return  Logger.Level.FULL;
	}

}
