package com.zimug.dongbb.cloud.aservice.rbac.controller;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.zimug.dongbb.cloud.starter.web.exception.AjaxResponse;
import com.zimug.dongbb.cloud.starter.web.exception.CustomExceptionType;
import org.springframework.web.bind.annotation.RequestParam;


public class SysuserControllerHandler {

  public static AjaxResponse pwdresetBlockHandler(@RequestParam Integer userId,
                                             BlockException blockException) {
    return AjaxResponse.error(CustomExceptionType.SYSTEM_ERROR,
            "尊敬的客户您好，系统服务繁忙，请稍后再试!");
  }

  public static AjaxResponse pwdresetFallback(@RequestParam Integer userId,Throwable e) {
    return AjaxResponse.error(CustomExceptionType.SYSTEM_ERROR,
            "尊敬的客户您好，系统服务繁忙，请稍后再试!（pwdresetFallBack）");
  }

  public static AjaxResponse defaultFallback(Throwable e) {
    return AjaxResponse.error(CustomExceptionType.SYSTEM_ERROR, e.getMessage());
  }

}
