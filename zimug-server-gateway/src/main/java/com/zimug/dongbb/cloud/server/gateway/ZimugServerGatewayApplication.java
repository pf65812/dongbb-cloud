package com.zimug.dongbb.cloud.server.gateway;

import com.alibaba.csp.sentinel.adapter.spring.webflux.callback.WebFluxCallbackManager;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class ZimugServerGatewayApplication {

	public static void main(String[] args) {
		WebFluxCallbackManager.setBlockHandler(new MySentinelBlockHandler());
		SpringApplication.run(ZimugServerGatewayApplication.class, args);
	}

}
